import dotenv from "dotenv";

dotenv.config();

const server = {
  host: process.env.SERVER_HOSTNAME,
  port: process.env.SERVER_PORT
};

const database = {
  host: process.env.DB_HOSTNAME,
  user: process.env.DB_USER!,
  password: process.env.DB_PASSWORD!,
  server: process.env.DB_SERVER!,
  name: process.env.DB_DATABASE!,
  port: parseInt(process.env.DB_PORT!, 10)
};

const config = {
  server,
  database
};

export default config;
