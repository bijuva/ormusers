import { User } from "./entity/user";
import { IUserRepository } from "./interface/iUserRepository";
import { User as ormUser } from "../models/user";

export class UserRepository implements IUserRepository {
  async findByPk(userId: number): Promise<User> {
    const user = await ormUser.findByPk(userId);

    if (user) return new User(user);
    else return new User(null);
  }

  findAll(): Promise<User[]> {
    throw new Error("Method not implemented.");
  }
  search(firstName: string, lastName: string): Promise<User[]> {
    throw new Error("Method not implemented.");
  }
  create(user: User): Promise<boolean> {
    throw new Error("Method not implemented.");
  }
  update(user: User): Promise<boolean> {
    throw new Error("Method not implemented.");
  }
  delete(id: number): Promise<boolean> {
    throw new Error("Method not implemented.");
  }
}
