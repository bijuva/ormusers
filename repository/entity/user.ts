export class User {
  private id: number = 0;
  private firstName: string = "";
  private lastName: string = "";
  private address: string = "";
  private age: number = 0;

  constructor(userObject: any) {
    if (userObject) {
      this.id = userObject.id;
      this.firstName = userObject.firstName;
      this.lastName = userObject.lastName;
      this.address = userObject.address;
      this.age = userObject.age;
    }
  }
  get Id() {
    return this.id;
  }
  get FirstName() {
    return this.firstName;
  }
  get LastName() {
    return this.lastName;
  }
  get Address() {
    return this.address;
  }
  get Age() {
    return this.age;
  }
}
