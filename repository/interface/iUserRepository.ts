import { User } from "../entity/user";

interface IRepo<T> {
  create(obj: T): Promise<boolean>;
  findByPk(id: number): Promise<T>;
  findAll(): Promise<Array<T>>;
  update(obj: T): Promise<boolean>;
  delete(id: number): Promise<boolean>;
}

export interface IUserRepository extends IRepo<User> {
  search(firstName: string, lastName: string): Promise<Array<User>>;
}
