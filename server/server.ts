import { DataTypes, Sequelize } from "sequelize";
import config from "../config";
import { Response } from "express";
import { httpStatus, sendToClient } from "./httpHandler";
import { User } from "../models/user";

export const seed = (res: Response) => {
  var sequelize = new Sequelize(
    config.database.name,
    config.database.user,
    config.database.password,
    {
      host: config.database.host,
      dialect: "mssql",
      dialectOptions: {
        options: {
          encrypt: true
        }
      }
    }
  );

  sequelize
    .authenticate()
    .then(() => {
      console.log("Connection has been established successfully.");
    })
    .catch(function (err) {
      console.log(`Unable to connect to the database: ${err}`);
      sendToClient(
        res,
        null,
        `Unable to connect to the database, Error: ${err}`,
        httpStatus.InternalServerError500
      );
    });

  const users = [
    {
      firstName: "Mary",
      lastName: "Barra",
      address: "300 Renaissance Ctr, Detroit, MI",
      age: 53
    },
    {
      firstName: "Satya",
      lastName: "Nadella",
      address: "1 Microsoft Way, Redmond, WA",
      age: 53
    },
    {
      firstName: "Tim",
      lastName: "Cook",
      address: "1 Apple Park Way, Cupertino, CA",
      age: 60
    },
    {
      firstName: "Indra",
      lastName: "Nooyi",
      address: "700 Anderson Hill Rd, Purchase, NY",
      age: 65
    }
  ];

  User.sync({ force: true })
    .then(function () {
      console.log("User table created.\n");
      User.bulkCreate(users);
      console.log("Records created.");
      sendToClient(res, users, "Database initialized.", httpStatus.Ok200);
    })
    .catch((err) => {
      sendToClient(
        res,
        null,
        `Not able to insert records! Error: ${err}`,
        httpStatus.InternalServerError500
      );
    });
};
