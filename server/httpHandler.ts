import { Response } from "express";
const contentType = { "Content-Type": "application/json" };

export const sendToClient = (
  res: Response,
  data?: any,
  message?: any,
  statusCode = httpStatus.Ok200
) => {
  const payload = {
    data: data,
    message: message,
    statusCode: statusCode
  };

  res.writeHead(statusCode, contentType);
  res.write(JSON.stringify(payload));
  res.end();
};

export const httpStatus = {
  Ok200: 200,
  Created201: 201,
  ClientError400: 400,
  Unauthorized401: 401,
  NotFound404: 404,
  MethodNotAllowed405: 405,
  InternalServerError500: 500
};
