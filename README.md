# ormUsers
----

A working example of a RESTful API project using Sequelizer, NodeJs, Express and Typescript developed in Visual Studio Code. 

## Introduction

Clone the project to your local folder and run "npm install" to install the necessary packages.
The project connects to a SQLServer. Please configure the server name, database, user id and password etc in the .env file in the root. 

## Steps

### Clone the repository

Follow the git instructions from bitbucket to download the code to your local computer.

```
https://bijuva@bitbucket.org/bijuva/ormusers.git
```
### Install the packages

```
npm install
```
### Install Typescript

```
npm install --save-dev typescript
```
### Build the project

```
npm run build
```
### Run the project

```
npm start
```
### Setup the database

Create a test database in your test SQLServer environment.

### Setup environment settings

Create a .env file in the rool folder of the application. Enter the information as below and save.

```
#
# database server
#
DB_USER=sa or your user id
DB_PASSWORD=your password
DB_SERVER=localhost or your test server name
DB_DATABASE=your test database
DB_PORT=1433 or db server port no
DB_HOSTNAME=localhost or db server host
#
# web server
#
SERVER_PORT=3000 or web server port
SERVER_HOSTNAME=localhost or web server domain name
```
## Seed the database
Use a browser, Postman or curl commands to seed the database.
It will create a "user" table with 4 rows in it. You can use curl as show below:

```
curl -v http://localhost:3000/seed/your-database-password
```
_Note: A password must be provided to reduce the risk of an accident. The password should match your database password._

### Running the tests

Use a browser, Postman or curl commands to consume the apis.

```
curl -v http://localhost:3000/users/1
```
_Note: Replace the url as needed._

### Insert a new user
Call the api: "/users" using POST. The request body should have an object as follows:
```
{ 
 "users": {
  "firstName": "fname",
  "lastName": "lname",
  "address": "addr",
  "age": 25
 }
}
```
### Find a user
Call the api: "/users/search" using POST. The request body should have an object as follows:
```
{ 
 "users": {
  "firstName": "fname",
  "lastName": "lname"
 }
}
```

### List of all APIs

* GET:    "/"                    -Returns a Welcome message     
* GET:    "/seed"                -Returns seed instructions
* GET:    "/seed/<password>"     -To seed the database
* GET:    "/users"               -List of users
* POST:   "/users"               -Insert a new user
* GET:    "/users/id"            -Find user by PK
* POST:   "/users/search"        -Search users by first and last name
* PUT:    "/users/id"            -Update a user *NOT IMPLEMENTED*
* DELETE: "/users/id"            -Delete a user *NOT IMPLEMENTED*

## Author

* **Biju Vargheese**


## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
