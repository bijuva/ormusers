const request = require("supertest");
const app = require("../build/controllers/user");

describe("Post Endpoints", () => {
  it("should create a new user", async () => {
    const res = await request(userController).post("/").send({
      firstName: "firstName",
      lastName: "lastName",
      age: 100,
      address: "address"
    });
    expect(res.status).toEqual(201);
    expect(res.body).toHaveProperty("post");
  });
});
