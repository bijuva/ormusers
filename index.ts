import express from "express";
import config from "./config";
import bodyParser from "body-parser";
import userController from "./controllers/user";
import dbController from "./controllers/db";

const app = express();

// parse json
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// middleware
app.use("/seed", dbController);
app.use("/users", userController);

app.get("/", (req, res) => {
  res.send("Welcome!");
});

app.listen(config.server.port, () =>
  console.log("Server running on port: " + config.server.port)
);

export default app;
