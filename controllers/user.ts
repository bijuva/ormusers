import express from "express";
import { User } from "../models/user";
import { httpStatus, sendToClient } from "../server/httpHandler";
import { UserRepository } from "../repository/userRepository";

const userController = express.Router();

// GET: list all users
userController.get("/", (req, res) => {
  (async () => {
    try {
      const users = await User.findAll();
      sendToClient(res, users, "Succes");
    } catch (err) {
      sendToClient(res, null, err, httpStatus.InternalServerError500);
    }
  })();
});

// POST: insert new user
userController.post("/", (req, res) => {
  // TODO: validation
  // get the values from body
  const user = req.body.user;

  if (!user) {
    sendToClient(
      res,
      null,
      "Missing 'user' object in request",
      httpStatus.ClientError400
    );
    return;
  }

  const firstName = user.firstName || "";
  const lastName = user.lastName || "";
  const address = user.address || "";
  const age = isNaN(Number(user.age)) ? 0 : +user.age;

  if (firstName == "") {
    sendToClient(
      res,
      null,
      new Error("First name is missing"),
      httpStatus.ClientError400
    );
    return;
  }

  const userParams = {
    firstName: firstName,
    lastName: lastName,
    age: age,
    address: address
  };

  (async () => {
    try {
      const newUser = await User.create(userParams);

      await newUser.save();
      sendToClient(res, userParams, "User created.", httpStatus.Created201);
    } catch (err) {
      sendToClient(
        res,
        null,
        "Error while creating user. " + err,
        httpStatus.InternalServerError500
      );
    }
  })();
});

// GET: find user by PK using repository pattern
userController.get("/:id", (req, res) => {
  const userId = isNaN(Number(req.params.id)) ? null : req.params.id;

  if (!userId) {
    sendToClient(res, null, "Missing user id", httpStatus.ClientError400);
    return;
  }

  (async () => {
    try {
      const userRepo = new UserRepository();
      const user = await userRepo.findByPk(+userId);

      if (user.Id === 0) {
        sendToClient(
          res,
          null,
          `User id ${userId} not found!`,
          httpStatus.NotFound404
        );
      } else sendToClient(res, user, "User found.");
    } catch (err) {
      sendToClient(res, null, err, httpStatus.InternalServerError500);
    }
  })();
});

// POST: search users by first and last name
userController.post("/search", (req, res) => {
  const user = req.body.user;

  if (!user) {
    sendToClient(
      res,
      null,
      "Missing 'user' object in request",
      httpStatus.ClientError400
    );
    return;
  }

  const firstName = user.firstName || null;
  const lastName = user.lastName || null;

  if (firstName == null || lastName == null) {
    sendToClient(
      res,
      null,
      "Missing 'firstName' or 'lastName' parameters",
      httpStatus.ClientError400
    );
    return;
  }

  (async () => {
    try {
      const users = await User.findAll({
        where: {
          firstName: firstName,
          lastName: lastName
        }
      });

      if (users) sendToClient(res, users, "Success", httpStatus.Ok200);
      else
        sendToClient(res, null, "No users found!", httpStatus.ClientError400);
    } catch (err) {
      sendToClient(
        res,
        null,
        "Error while pulling users. " + err,
        httpStatus.ClientError400
      );
    }
  })();
});

userController.put("/:id", (req, res) => {
  sendToClient(
    res,
    null,
    "Update a user. Not implemented.",
    httpStatus.MethodNotAllowed405
  );
});

userController.delete("/:id", (req, res) => {
  sendToClient(
    res,
    null,
    "Delete a user. Not implemented.",
    httpStatus.MethodNotAllowed405
  );
});

export default userController;
