import express from "express";
import config from "../config";
import { httpStatus, sendToClient } from "../server/httpHandler";
import { seed } from "../server/server";

const dbController = express.Router();

dbController.get("/", (req, res) => {
  sendToClient(
    res,
    null,
    "To seed the database, use end point '/seed/your_database_password'",
    httpStatus.ClientError400
  );
});

dbController.get("/:pwd", (req, res) => {
  const pwd = req.params.pwd;

  // seeding needs database password to be supplied.
  if (pwd !== config.database.password) {
    sendToClient(
      res,
      null,
      "Failed. Wrong password!",
      httpStatus.Unauthorized401
    );
    return;
  } else {
    seed(res);
  }
});

export default dbController;
