import { Sequelize, DataTypes } from "sequelize";
import config from "../config";

const sequelize = new Sequelize(
  config.database.name,
  config.database.user,
  config.database.password,
  {
    host: config.database.host,
    dialect: "mssql",
    dialectOptions: {
      options: {
        encrypt: true
      }
    }
  }
);

export const User = sequelize.define("user", {
  id: { type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true },
  firstName: { type: DataTypes.STRING },
  lastName: { type: DataTypes.STRING },
  age: { type: DataTypes.INTEGER },
  address: { type: DataTypes.STRING }
});
